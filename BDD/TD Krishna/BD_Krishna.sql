-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 31 Mai 2016 à 19:13
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  krishna
--

-- --------------------------------------------------------

--
-- Structure de la table CATEGORIE
--

CREATE TABLE CATEGORIE (
  CodeC varchar(5) NOT NULL,
  LibelleC varchar(40) NOT NULL,
  PRIMARY KEY (CodeC)
  ) ENGINE=INNODB DEFAULT CHARSET=latin1;

--
-- Contenu de la table CATEGORIE
--

INSERT INTO CATEGORIE (CodeC, LibelleC) VALUES
('B', 'Boissons'),
('E', 'Entrées'),
('P', 'Plats'),
('D', 'Desserts'),
('V', 'Vins');

-- --------------------------------------------------------

--
-- Structure de la table TYPEPLAT
--

CREATE TABLE TYPEPLAT (
  CodeTP varchar(5) NOT NULL,
  LibelleTP varchar(40) NOT NULL,
  CodeC varchar(5) NOT NULL,
  PRIMARY KEY (CodeTP)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

--
-- Contenu de la table TYPEPLAT
--

INSERT INTO TYPEPLAT (CodeTP, LibelleTP, CodeC) VALUES
('1', 'alcool', 'B'),
('2', 'soda', 'B'),
('3', 'boisson indienne', 'B'),
('4', 'entrée poulet', 'E'),
('5', 'entrée agneau', 'E'),
('6', 'entrée fruits de mer', 'E'),
('7', 'entrée légumes', 'E'),
('8', 'entrée salade', 'E'),
('9', 'pain indien', 'E'),
('10', 'plat agneau', 'P'),
('11', 'plat poulet', 'P'),
('12', 'plat boeuf', 'P'),
('13', 'riz', 'P'),
('14', 'plat fruit de mer', 'P'),
('15', 'plat gambas', 'P'),
('16', 'plat poisson', 'P'),
('17', 'plat légumes', 'P'),
('18', 'fried rice', 'P'),
('19', 'briyani', 'P'),
('20', 'dessert', 'D'),
('21', 'glace', 'D'),
('22', 'thé', 'D'),
('23', 'café', 'D')
;
-- --------------------------------------------------------

--
-- Structure de la table PLAT
--

CREATE TABLE PLAT (
  CodeP varchar(5) NOT NULL,
  LibelleP varchar(40) NOT NULL,
  Description varchar(50) NOT NULL,
  Prix decimal(5,2) NOT NULL,
  Disponible tinyint(1) DEFAULT NULL,
  CodeType varchar(5) NOT NULL,
  PRIMARY KEY (CodeP)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

--
-- Contenu de la table PLAT
--

INSERT INTO PLAT (CodeP, LibelleP, Description, Prix, Disponible, CodeType) VALUES
('01', 'PORTO', '', '3.75', 1, '1'),
('02', 'MARTINI', '', '4.00', 1, '1'),
('03', 'WHISKY', '', '5.00', 1, '1'),
('04', 'WHISKY COCA', '', '5.75', 1, '1'),
('05', 'GIN TONIC', '', '4.75', 1, '1'),
('06', 'RICARD', '', '3.50', 1, '1'),
('07', 'CHIVAS', '', '6.00', 1, '1'),
('08', 'JUS D''ORANGE 25cl', '', '2.75', 1, '2'),
('09', 'JUS DE MANGUE 25cl', '', '2.80', 1, '2'),
('10', 'JUS D''ANANAS 25cl', '', '2.75', 1, '2'),
('11', 'COCA COLA 33cl', '', '3.75', 1, '2'),
('12', 'SPRITE 33cl', '', '3.00', 1, '2'),
('13', 'ORANGINA 25cl', '', '2.75', 1, '2'),
('14', 'SCHEWEPPES 25cl', '', '2.50', 1, '2'),
('15', 'FANTA 33cl', '', '3.10', 1, '2'),
('16', 'HEINE KEN 25cl', '', '3.40', 1, '1'),
('17', 'BIERE 1664 25cl', '', '3.40', 1, '1'),
('18', 'BIERE INDIENNE 33cl', '', '4.20', 1, '1'),
('19', 'BADOT 1L', '', '4.75', 1, '2'),
('20', 'EVIAN 50cl', '', '2.85', 1, '2'),
('21', 'SAN PELLEGRINO 50cl', '', '2.85', 1, '2'),
('22', 'PERRIER 33cl', '', '2.90', 1, '2'),
('23', 'LASSY', 'Nature, sucré ou salé', '3.50', 1, '3'),
('24', 'LASSY BANANE', '', '4.00', 1, '3'),
('25', 'LASSY MANGUE', '', '4.75', 1, '3'),
('26', 'LASSY SIROP DE ROSE', '', '4.25', 1, '3'),
('27', 'POULET TANDOORI', 'Poulet aux épices, cuit à la broche au tandoori', '5.75', 1, '4'),
('28', 'POULET TIKKA ', 'Poulet aux épices, cuit à la broche au tandoori', '5.75', 1, '4'),
('29', 'CAILLE TANDOORI (1 pièce)', 'Caille marinée et ses épices', '5.00', 1, '4'),
('30', 'MIXED GRILLE (pour 2 personnes)', 'Caille, agneau, poulet, saumon, mix pakora', '15.00', 1, '4'),
('31', 'SAMOSA LEGUMES', 'Chaussons aux pommes de terre, petits pois et ses ', '4.90', 1, '7'),
('32', 'AUBERGINES PAKORA ', 'Beignets d''aubergines à la farine de pois chiche', '4.10', 1, '7'),
('33', 'OIGNONS BADJI', 'Beignets d''oignons à la farine de pois chiche', '4.75', 1, '7'),
('34', 'MIX PAKORA', 'Beignets avec des oignons, des pommes de terre et ', '4.90', 1, '7'),
('35', 'BEIGNETS KRISHNA', 'Cutlets, samosa légumes, agneau roll et mix pakora', '6.75', 1, '7'),
('36', 'RAITHA', 'Yaourt aux légumes et épices', '3.75', 1, '7'),
('37', 'CREVETTES RAITHA', 'Yaourt avec épices et crevettes décortiquées', '4.50', 1, '7'),
('38', 'AGNEAU TIKKA', 'Gigot d''agneau désossé, légèrement mariné aux épic', '7.15', 1, '5'),
('39', 'AGNEAU ROLL', 'Rouleau avec colombo d''agneau et pommes de terre', '5.75', 1, '5'),
('40', 'SAUMON TANDOORI', 'Filets de saumon marinés dans une sauce et cuit au', '7.00', 1, '6'),
('41', 'GAMBAS TANDOORI', 'Gambas marinées dans du yaourt et composée d''épice', '12.00', 1, '6'),
('42', 'KANAVAI PAKORA', 'Beignets de calamars', '4.95', 1, '6'),
('43', 'CUTLETS ', 'Boulettes de poissons', '4.50', 1, '6'),
('44', 'SALADE VERTE', 'Salade composée d''oignons, de tomates et de concom', '4.00', 1, '8'),
('45', 'SALADE KRISHNA', 'Salade composée d''oignons, de tomates, de concombr', '6.00', 1, '8'),
('46', 'SALADE DE CREVETTES', 'Salade composée de tomates, de concombres et de cr', '5.75', 1, '8'),
('47', 'SALADE DE POULET', 'Salade composée de tomates, de concombres, d''oigno', '6.90', 1, '8'),
('48', 'NAN', 'Pain à pâte levée nature', '2.00', 1, '9'),
('49', 'GARLIC CHEESE NAN', 'Pain à pâte levée à l''ail, coriandre frais et du f', '4.50', 1, '9'),
('50', 'NAN GARLIC', 'Pain à pâte levée à l''ail', '3.75', 1, '9'),
('51', 'NAN OIGNONS', 'Pain à pâte levée à l''oignon', '3.50', 1, '9'),
('52', 'NAN FROMAGE', 'Pain à pâte levée, farci au fromage', '3.90', 1, '9'),
('53', 'MAXI NAN FROMAGE', 'Nan ''double fromage''', '6.20', 1, '9'),
('54', 'NAN LEGUMES', 'Fait Maison', '4.75', 1, '9'),
('55', 'CREVETTES NAN', 'Fait Maison', '4.90', 1, '9'),
('56', 'NAN VINDALO', 'Pain à pâte levée avec coriandre, oignons, et de l', '6.00', 1, '9'),
('57', 'AGNEAU AU CURRY', 'Curry d''agneau, tomates, oignons et épices', '11.00', 1, '10'),
('58', 'AGNEAU MADRAS', 'Curry d''agneau, tomates, piment vert et épices (tr', '12.75', 1, '10'),
('59', 'AGNEAU VINDALO', 'Curry aux quatre épices (relevé)', '12.75', 1, '10'),
('60', 'AGNEAU TIKKA MASALA', 'Agneau préparé au tandoori, cuit dans une sauce lé', '14.25', 1, '10'),
('61', 'AGNEAU KOFTA', 'Boulettes de viande hachées, farci au fromage avec', '14.90', 1, '10'),
('62', 'AGNEAU KRISHNA', 'Agneau préparé au tandoori, cuit dans une sauce lé', '13.50', 1, '10'),
('63', 'AGNEAU JHAL FREZI', 'Morceaux d''agneau désossés avec poivron vert, toma', '13.00', 1, '10'),
('64', 'AGNEAU PALAK', 'Viande en morceaux avec de l''épinard, des épices, ', '12.90', 1, '10'),
('65', 'AGNEAU ROGAN GOST', 'Curry d''agneau avec des légumes', '12.90', 1, '10'),
('66', 'AGNEAU DALL', 'Agneau au curry avec tomates, oignons, beurre, noi', '12.90', 1, '10'),
('67', 'AGNEAU KANDARI', 'Agneau avec des légumes et du lait de coco', '13.50', 1, '10'),
('68', 'AGNEAU BENGALI', 'Curry moyennement relevé avec aubergines et crème ', '13.00', 1, '10'),
('69', 'AGNEAU SHAI KORMA', 'Légèrement épicé, noix de cajou, pistache, amandes', '12.90', 1, '10'),
('70', 'AGNEAU KASHMIRI', 'Agneau au curry avec de la crème fraîche, des noix', '7.00', 1, '10'),
('71', 'GARLIC CHICKEN', 'Ail, oignons, crème fraîche, crème de cajou et épi', '12.70', 1, '11'),
('72', 'POULET MADRAS', 'Poulet au curry avec tomates, oignons, piment vert', '11.00', 1, '11'),
('73', 'POULET KANDARI', 'Poulet au curry avec des légumes et du lait de coc', '12.25', 1, '11'),
('74', 'POULET KORMA', 'Légèrement épicé avec des noix de cajou, pistache,', '11.00', 1, '11'),
('75', 'POULET VINDALO', 'Poulet au curry aux quatre épices', '11.25', 1, '11'),
('76', 'POULET KRISHNA', 'Poulet grillé au tandoori et au curry, avec tomate', '12.50', 1, '11'),
('77', 'POULET TIKKA MASALA', 'Poulet mariné dans une sauce au yaourt, aux herbes', '13.00', 1, '11'),
('78', 'POULET KASHMIRI', 'Poulet au curry, avec de la crème fraîche, des noi', '12.00', 1, '11'),
('79', 'BUTTER CHICKEN', 'Poulet grillé au four puis cuisiné au curry', '12.90', 1, '11'),
('80', 'POULET JHAL FREZI', 'Poulet avec oignons, poivrons, coriandre, oeuf et ', '12.00', 1, '11'),
('81', 'POULET SHAI KORMA', 'Poulet désossé avec de la crème fraîche, des amand', '12.00', 1, '11'),
('82', 'POULET BENGALI', 'Poulet au curry avec des aubergines et des épices', '12.75', 1, '11'),
('83', 'POULET CHAMPIGNONS', 'Poulet au curry préparé avec des champignons, oing', '12.25', 1, '11'),
('84', 'PALAK CHICKEN', 'Poulet au curry préparé avec oignons, tomates, cor', '12.50', 1, '11'),
('85', 'RIZ BASMATI', 'Riz Basmati au safran', '4.50', 1, '13'),
('86', 'RIZ KASHMIRI', 'Riz Basmati avec des fruits exotiques', '5.50', 1, '13'),
('87', 'FRIED RICE', 'Riz Basmati avec des légumes variés et ghee', '5.50', 1, '13'),
('88', 'BOULETTES DE BOEUF VINDALO', 'Curry au quatre épices, oignons, tomates, coriandr', '11.00', 1, '12'),
('89', 'BOULETTES DE BOEUF MADRAS', 'Tomates, oignons, gingembre, ail, coriandre, pimen', '11.50', 1, '12'),
('90', 'BOULETTES DE BOEUF KORMA', 'Légèrement épicé avec oignons, tomates, coriandre,', '12.50', 1, '12'),
('91', 'CREVETTES PRINTANIERE', 'Sauté de crevettes aux légumes sur plaque chauffan', '15.90', 1, '14'),
('92', 'MALAI DE CREVETTES', 'Crevettes décortiquées cuites dans une sauce légèr', '14.75', 1, '14'),
('93', 'CREVETTES VINDALO', 'Crevettes au curry, moyennement épicé à la sauce v', '14.50', 1, '14'),
('94', 'CREVETTES KORMA', 'Crevettes au curry, avec noix de cajou, crème fraî', '14.75', 1, '14'),
('95', 'CREVETTES TIKKA MASALA', 'Crevettes légèrement épicées avec gingembre, yaour', '15.90', 1, '14'),
('96', 'CREVETTES BENGALI', 'Curry de crevettes avec aubergines', '15.00', 1, '14'),
('97', 'CREVETTES MADRAS', 'Tomates, oignons, gingembre, ail, coriandre, pimen', '14.90', 1, '14'),
('98', 'DALL CURRY', 'Lentilles au curry et au beurre', '9.75', 1, '17'),
('99', 'DALL MADRAS', 'Lentilles au curry avec tomates, oignons, piment v', '10.00', 1, '17'),
('100', 'PALAK CURRY', 'Curry d''épinards avec sauce de noix de cajou, cori', '9.90', 1, '17'),
('101', 'PALAK PANNER', 'Curry d''épinards avec crème fraîche, tomates, oign', '10.00', 1, '17'),
('102', 'AUBERGINES CURRY', 'Aubergines au curry, coriandre frais, crème fraîch', '9.90', 1, '17'),
('103', 'AUBERGINES MADRAS', 'Aubergines au curry, tomates, oignons, piment vert', '10.25', 1, '17'),
('104', 'SABJI KORMA', 'Légumes variés avec crème fraîche, noix de cajou e', '9.90', 1, '17'),
('105', 'SABJI MADRAS', 'Légumes variés, coriandre frais, piment vert et ép', '10.50', 1, '17'),
('106', 'CHAMPIGNONS CURRY', 'Champignons avec crème fraîche, tomates, oignons, ', '11.00', 1, '17'),
('107', 'ALLO GOBI', 'Choux fleur avec des pommes de terre et des épices', '9.75', 1, '17'),
('108', 'BOMBAY ALLO', 'Pommes de terre, haricots burre, lait de coco et é', '10.50', 1, '17'),
('109', 'GAMABAS VINDALO', 'Gambas au curry moyennement épicé à la sauce vinda', '15.90', 1, '15'),
('110', 'GAMBAS KANDARI', 'Gambas avec des légumes et du lait de coco', '16.50', 1, '15'),
('111', 'GAMBAS MASALA', 'Préparé spécialement par le chef', '16.50', 1, '15'),
('112', 'GAMBAS MADRAS', 'Gambas aux fines herbes indiennes avec tomates, co', '15.90', 1, '15'),
('113', 'FISH TIKKA MASALA', 'Saumon préparé au tandoori, cuit dans une sauce lé', '14.75', 1, '16'),
('114', 'FISH MASALA', 'Filets de saumon au curry avec tomates, oignons, n', '12.00', 1, '16'),
('115', 'FISH VINDALO', 'Filets de daumon au curry moyennement épicés à la ', '12.00', 1, '16'),
('116', 'FISH MADRAS', 'Filets de saumon aux fines herbes indiennes avec t', '12.90', 1, '16'),
('117', 'FISH BENGALI', 'Curry de poissons avec aubergines dans une sauce p', '14.00', 1, '16'),
('118', 'COQUILLES ST-JACQUES', 'Curry de coquilles St-Jacques avec noix de cajou, ', '15.25', 1, '14'),
('119', 'POULET', '', '12.00', 1, '18'),
('120', 'AGNEAU', '', '13.75', 1, '18'),
('121', 'CREVETTES', '', '14.75', 1, '18'),
('122', 'MIXED', 'Agneau, poulet et crevettes', '15.00', 1, '18'),
('123', 'LEGUMES', '', '12.00', 1, '19'),
('124', 'POULET', '', '13.00', 1, '19'),
('125', 'AGNEAU', '', '14.00', 1, '19'),
('126', 'CREVETTES', '', '15.00', 1, '19'),
('127', 'MIXED', '', '17.00', 1, '19'),
('129', 'CREME A LA MANGUE', 'Fait Maison', '4.75', 1, '20'),
('130', 'KESARI', 'Gâteau à la semoule, amandes, noix de cajou et car', '3.75', 1, '20'),
('131', 'GULAB JAMMON', 'Petits beignets parfumés à l''eau de rose', '4.50', 1, '20'),
('132', 'SALADE DE FRUITS', 'Fruits frais', '5.20', 1, '20'),
('133', 'KULFI', 'Glace indienne à la pistache, aux noix de cajou et', '4.75', 1, '21'),
('134', 'GLACE ROYALE', 'Papaye, fruits du jacquier, mangue, nappé d''un cou', '5.50', 1, '21'),
('135', 'GLACE (deux boules)', 'Vanille, chocolat, café, fraise, noix de coco, pis', '3.30', 1, '21'),
('136', 'SORBET (deux boules)', 'Mangue, citron vert, fruit de la passion, ananas, ', '3.50', 1, '21'),
('137', 'NOIX DE COCO GIVREE', '', '5.50', 1, '21'),
('138', 'CITRON GIVREE', 'Fait Maison', '5.00', 1, '21'),
('139', 'ORANGE GIVREE', 'Fait Maison', '5.00', 1, '21'),
('140', 'THE A LA MENTHE', '', '2.50', 1, '22'),
('141', 'THE MAISON', 'Thé à la cardomome', '2.50', 1, '22'),
('142', 'MASALA CHAI', '', '3.50', 1, '22'),
('143', 'CAFE OU DECAFEINE', '', '1.75', 1, '22'),
('144', 'CAFE AU LAIT', '', '2.00', 1, '22')
;

-- --------------------------------------------------------

--
-- Structure de la table MENU
--

CREATE TABLE MENU (
  CodeM varchar(5) NOT NULL,
  LibelleM varchar(20) NOT NULL,
  Prix decimal(5,2) NOT NULL,
  photo varchar(20) NOT NULL,
  PRIMARY KEY (CodeM)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

--
-- Contenu de la table MENU
--

INSERT INTO MENU (CodeM, LibelleM, Prix, photo) VALUES
('M18', 'MENU découverte', '18.00', ''),
('M22', 'MENU gourmand', '22.00', ''),
('M35', 'MENU gourmet', '35.00', '')
;

-- --------------------------------------------------------

--
-- Structure de la table composer
--

CREATE TABLE composer (
  CodeM varchar(5) NOT NULL,
  CodeP varchar(5) NOT NULL,
  PRIMARY KEY (CodeM,CodeP)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

--
-- Contenu de la table composer
--

INSERT INTO composer (CodeM, CodeP) VALUES
('M18', 28),
('M18', 34),
('M18', 45),
('M18', 74),
('M18', 88),
('M18', 100),
('M18', 130),
('M18', 135),
('M22', 27),
('M22', 42),
('M22', 46),
('M22', 69),
('M22', 75),
('M22', 90),
('M22', 133),
('M22', 136);

-- --------------------------------------------------------

--
-- Structure de la table utilisateur
--

CREATE TABLE utilisateur (
  NumU int(5) NOT NULL AUTO_INCREMENT,
  PseudoU varchar(30) NOT NULL,
  MailU varchar(40) NOT NULL,
  MdpU varchar(25) NOT NULL,
  TelephoneU varchar(16) NOT NULL,
  PRIMARY KEY (NumU)
 ) ENGINE=INNODB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table utilisateur
--

INSERT INTO utilisateur (NumU, PseudoU, MailU, MdpU, TelephoneU) VALUES
(1, 'Pierre', 'pierredu33@hotmail.fr', 'mdp1', '0698256145' ),
(7, 'Stk', 'kirupro@outlook.fr', 'mdp7', '0659193505'),
(8, 'Stk', 'kirupro@outlook.fr', 'mdp8', '0659193505');


-- --------------------------------------------------------

--
-- Structure de la table COMMANDE
--

CREATE TABLE COMMANDE (
  NumC int(5) NOT NULL AUTO_INCREMENT,
  DateC date NOT NULL,
  DateRecup date NOT NULL,
  HeureRecup varchar(5) NOT NULL,
  NumU int(5) NOT NULL,
  PRIMARY KEY (NumC)
) ENGINE=INNODB  DEFAULT CHARSET=latin1 ;

--
-- Contenu de la table demande
--

INSERT INTO COMMANDE (NumC, DateC, DateRecup, HeureRecup, NumU) VALUES
(1, '2019-03-10','2019-03-10','19:30', 1);


-- --------------------------------------------------------

--
-- Structure de la table COMMANDER PLAT
--

CREATE TABLE COMMANDER_P (
  NumC int(5) NOT NULL ,
  CodeP varchar(5) NOT NULL,
  QttePlat int(5) ,
  PRIMARY KEY (NumC, CodeP)
) ENGINE=INNODB  DEFAULT CHARSET=latin1 ;

--
-- Contenu de la table COMMANDER_P
--

-- --------------------------------------------------------

--
-- Structure de la table COMMANDER MENU
--

CREATE TABLE COMMANDER_M (
  NumC int(5) NOT NULL ,
  CodeM varchar(5) NOT NULL,
  QtteMenu int(5) ,
  PRIMARY KEY (NumC, CodeM)
) ENGINE=INNODB  DEFAULT CHARSET=latin1 ;

--
-- Contenu de la table COMMANDER_M
--

-- --------------------------------------------------------

--
-- Structure de la table reservation
--

CREATE TABLE RESERVATION (
  NumR int(5) NOT NULL AUTO_INCREMENT,
  DateR date NOT NULL,
  DateResa date NOT NULL,
  HeureResa time NOT NULL,
  NbPersonnes int(2) NOT NULL,
  Confirmee tinyint(1) NOT NULL,
  NumU int(5) NOT NULL,
  PRIMARY KEY (NumR)
) ENGINE=INNODB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table reservation
--

INSERT INTO RESERVATION (NumR, DateR, DateResa, HeureResa, NbPersonnes, Confirmee, NumU) VALUES
(1, '0000-00-00', '0000-00-00', '12:36:00', 3, 0, 1),
(2, '0000-00-00', '0000-00-00', '12:36:00', 3, 0, 1),
(3, '0000-00-00', '2019-12-03', '12:36:00', 3, 0, 1),
(4, '2019-03-10', '2019-12-03', '19:50:00', 8, 0, 7),
(5, '2019-03-10', '2019-12-03', '19:50:00', 8, 0, 7);

ALTER TABLE PLAT
ADD FOREIGN KEY(CodeType) REFERENCES TYPEPLAT(codeTP) ; 

ALTER TABLE TYPEPLAT
ADD FOREIGN KEY (CodeC) REFERENCES CATEGORIE(CodeC) ; 

ALTER TABLE COMPOSER
ADD FOREIGN KEY (CodeP) REFERENCES PLAT(CodeP) ,
ADD FOREIGN KEY (CodeM) REFERENCES MENU(CodeM) ;

ALTER TABLE RESERVATION
ADD FOREIGN KEY(NumU) REFERENCES UTILISATEUR(NumU) ; 

ALTER TABLE COMMANDE
ADD FOREIGN KEY(NumU) REFERENCES UTILISATEUR(NumU) ; 

ALTER TABLE COMMANDER_P 
ADD FOREIGN KEY(CodeP) REFERENCES PLAT(CodeP) , 
ADD FOREIGN KEY(NumC) REFERENCES COMMANDE(NumC) 
;

ALTER TABLE COMMANDER_M 
ADD FOREIGN KEY(CodeM) REFERENCES MENU(CodeM) , 
ADD FOREIGN KEY(NumC) REFERENCES COMMANDE(NumC)
;

